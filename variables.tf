variable "lets_encrypt_email" {
  description = "The email to use for lets encrypt registration"
  type        = string
}

variable "linode_token" {
  description = "The token to use to auth with the linode API"
  type        = string
  default     = null
}
