# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.2.0"
  hashes = [
    "h1:rxDS2QQuG/M9aRSKlvW2oHsp5eGAoB1J9KZthCOVbeQ=",
    "zh:01341dd1e9cc7e7f6999e11e7473bcdca2dd72dd27f91beed1f4fb599a15dfba",
    "zh:20e86c9eccd3a81ef5ac243af31b61fc4d2d679437384bd0870e92fa1b3ed6c9",
    "zh:22a71127c5dbea4f62edb5bcf00b5c163de04aa19d45a7a1f621f973ffd09d20",
    "zh:28ab7c84a5f8ed82fc520668db93d650571ddf59d98845cb18a1fa1a7888efc0",
    "zh:3985a30929ad8fdc6b94f0e1cbd62a63db75ee961b8ba7db1cf4bfd29e8009ff",
    "zh:477d92e26ba0c906087a5dd827ac3917dad7d5af770ee0ab4b08d0f273150586",
    "zh:750928ec5ef54b2090bd6a6d8a19630a8712bbbccc0429251e88ccd361c1d3c0",
    "zh:a615841fd90094bddc1269127e501fa60453c441b9548ff73752fe14efc38ed0",
    "zh:e762aca7883374fa255efba50f5bdf791fece7d61e3920e593fb1a2cbb598981",
    "zh:f76f372ead52948ca53610b371cb80c80ebcf058ef0a5c0ce9f0ce38dcc9a8eb",
    "zh:fa36fe93ed977f4478cc6547ec3c45c28e56f10632e85446b0c3d71449f8c4bb",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.3.2"
  hashes = [
    "h1:X+wKrZNYF+qtoBNcMD3cL0+bXSWUDZ7RpHm+2vAwvhE=",
    "zh:10f71c170be13538374a4b9553fcb3d98a6036bcd1ca5901877773116c3f828e",
    "zh:11d2230e531b7480317e988207a73cb67b332f225b0892304983b19b6014ebe0",
    "zh:3317387a9a6cc27fd7536b8f3cad4b8a9285e9461f125c5a15d192cef3281856",
    "zh:458a9858362900fbe97e00432ae8a5bef212a4dacf97a57ede7534c164730da4",
    "zh:50ea297007d9fe53e5411577f87a4b13f3877ce732089b42f938430e6aadff0d",
    "zh:56705c959e4cbea3b115782d04c62c68ac75128c5c44ee7aa4043df253ffbfe3",
    "zh:7eb3722f7f036e224824470c3e0d941f1f268fcd5fa2f8203e0eee425d0e1484",
    "zh:9f408a6df4d74089e6ce18f9206b06b8107ddb57e2bc9b958a6b7dc352c62980",
    "zh:aadd25ccc3021040808feb2645779962f638766eb583f586806e59f24dde81bb",
    "zh:b101c3456e4309b09aab129b0118561178c92cb4be5d96dec553189c3084dca1",
    "zh:ec08478573b4953764099fbfd670fae81dc24b60e467fb3b023e6fab50b70a9e",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version = "1.18.0"
  hashes = [
    "h1:+8f4i5sj2WOd5bdWSPUafHdN+s1Fgq9/l1+Rc3TMf7U=",
    "zh:0ead391cba4eccff9d46c91e9260ce5e2ccfd69e2aebef253768ce29e2de3a7d",
    "zh:27708a55d1ba1594086c2015441243a38a608f68ea2f82f1d759c6baf2a0df14",
    "zh:3d355a270e7eaeafd5044a326c527c23742b312376368e1019e3caa779cdbc91",
    "zh:41dde82124e6c2e2640ef2963fe4f6faf16f8e8b82e7dbaebfdec7b781f5455a",
    "zh:51e9139cdc1386053c6834585139dc74d6fb7653a00b495377bc445b5e532218",
    "zh:6ba6560bf23736a2a6e4c0899afd2c25cac6697d90cf2573449fe9b655f87920",
    "zh:79c1fa8e3a8705eee73f171229ff47688deaff8468cdf28fddaafe5aef7e2d8d",
    "zh:80b008ded1c71313c4f76e5569142e3a56b866f7693e57270d15f13fc7af1e14",
    "zh:b0ebb1e83e8d999dc1d8feecf9c1e293cd61fe72271610284fdcce46d4a8a7ed",
    "zh:bdaa786f0381ccd61404ea1835733e852e9747f1daf9a63bd4149073dbce85b6",
    "zh:c67cd9e8d4880dfa6cbbd25aa7fcd9c07a76f4801180ac3988ff3f84ede6181f",
    "zh:c8ee62dfd07d83dd362b8ba5f13a957e1ec8107b22ac168da4fa8470c4537a33",
    "zh:cf7bdc5eac5df6cfc6ab5c7cafaba72b6bf5a155017e25edc6d9dc192bb6d2ed",
  ]
}
