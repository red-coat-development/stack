
resource "helm_release" "ingress" {
  name             = "ingress"
  namespace        = "ingress-nginx"
  create_namespace = true
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
}

data "kubernetes_service" "ingress" {
  metadata {
    name      = "ingress-ingress-nginx-controller"
    namespace = helm_release.ingress.namespace
  }
}

locals {
  ingress_load_balancer = data.kubernetes_service.ingress.status[0].load_balancer[0].ingress[0].hostname
}
