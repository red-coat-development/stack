resource "linode_lke_cluster" "main" {
  label       = "main"
  k8s_version = "1.21"
  region      = "eu-west"
  tags        = ["prod"]

  pool {
    type  = "g6-standard-2"
    count = 1
  }
}

# Linode's authentication model is very basic, and their LKE clusters
# just spit out a kubeconfig file with a lke-admin service account
# token. We have to do this hacky nonsense to pull out the relavent
# details for the helm provider.
locals {
  cluster_auth_data      = yamldecode(base64decode(linode_lke_cluster.main.kubeconfig))
  cluster                = local.cluster_auth_data.clusters[0].cluster
  cluster_host           = nonsensitive(replace(local.cluster.server, ":443", ""))
  cluster_ca_certificate = base64decode(local.cluster.certificate-authority-data)
  cluster_token          = local.cluster_auth_data.users[0].user.token
}
